var config = {
	port: 80,
	cwd: './',

	proxy_port: 1080,
	proxy_hosts: [
		// '127.0.0.1 localhost'
	],
	proxy_rules: [
		// rule: match-url target-server
		// The latter rule would overwrite the previous rules
		// 'http://cdn\\d?.online.com http://11.22.33.44', // #1 remote address
		// 'http://cdn(\\d)?.online.com/LJPoseidon/(.+) http://cdn$1.dev.com:8080' // #2 local address
	]
};

module.exports = config;