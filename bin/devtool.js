#!/usr/bin/env node
var program = require('commander');
var fs = require('fs');
var nativePath = require('path');
var util = require('util');
var cwd = process.cwd();
var packageJSON = require('../package.json');
var config = require('../config.js');
var devtool = require('../lib/devtool.js');
var extend = require('../lib/modules/deep-extend.js');
var serverRoot = cwd;
function statringAsBoolean (s) {
	if (s === 'false') {
		return false;
	}
	return true;
}

var localConfigPath = nativePath.join(process.cwd(), 'devtool.json');
var localConfig;
if (fs.existsSync(localConfigPath)) {
	localConfig = fs.readFileSync(localConfigPath).toString();
	if (localConfig) {
		extend(config, JSON.parse(localConfig));
	}
}


program
	.version( packageJSON.version )
	.usage('[options] directory')
	.arguments('<dir>')
	.action(function (dir) {
		config.cwd = nativePath.resolve(cwd, dir);
	})
	.option('-p, --port <port>', 'static server port', parseInt)
	.option('-c, --combo [true|false]', 'use combo server', statringAsBoolean)
	.option('-s, --static [true|false]', 'use static server', statringAsBoolean)
	// .option('-b, --baseUrl <url>', 'static server base from some base url')
	.option('-r, --proxy [false|true]', 'use proxy server', statringAsBoolean)
	.option('-o, --proxy_port <port>', 'proxy server port', parseInt)
	.parse(process.argv);


'port combo static proxy proxy_port'.split(' ').forEach(function(key) {
	if ( program.hasOwnProperty(key) && typeof program[key] !== 'undefined' ) {
		config[key] = program[key];
	}
});
devtool(config);
