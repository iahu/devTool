module.exports = function (req, res, next, conf) {
	if (req.method !== 'GET') {
		return next();
	}
	var COMBO_SERVER = 'combosvs';
	var VIA = 'devtool';
	var host = req.headers.host;
	var appConfig = conf || require('../config.js');
	var baseUrl = appConfig.cwd;
	var hostObj = host.split(':');
	// if ( appConfig.hostname.split(' ').indexOf(hostObj[0]) < 0 &&
	// 	(hostObj[1] || 80) === appConfig.port ) {
	// 	return next();
	// }
	var comboCSS = require('../modules/combo-css');
	var comboJS = require('../modules/combo-js');
	var logger = require('../modules/logger.js');
	var _ = require('lodash');
	var nativePath = require('path');
	var querystring = require('querystring');
	var originPathname = req._parsedUrl.pathname;
	var parsed = nativePath.parse(req._parsedUrl.pathname);
	var ext = parsed.ext.slice(1);
	var spliter = /[;,]/g;
	var CSS_PATTERN = /^(less|css|sass)$/;
	var prefix = parsed.dir.slice(1);
	var pathnameList = originPathname.split(spliter);
	var allSameExt = _.all(pathnameList, function (pn) {
		return pn.split('.').pop() == ext;
	});
	var qs = querystring.parse(req._parsedUrl.query);
	var rMap = {fullrequire: 'fullrequire', minirequire: 'minirequire'};
	if (pathnameList.length < 2 && !qs.hasOwnProperty('r') ) {
		logger('[info] static ' + req.url);
		return next();
	}
	if (!allSameExt) {
		// var exp = new RegExp('\\.'+ ext + '$');
		pathnameList = pathnameList.map(function (pn) {
			var p = nativePath.parse(pn);
			return !p.ext ? pn + '.' + ext : pn;
			// return exp.exec(pn) ? pn : pn + '.' + ext;
		});
	}
	switch(ext) {
		case 'js':
			if (qs.r) {
				pathnameList.unshift( '/'+ (rMap[qs.r] || 'minirequire') + '.js');
			}
			break;
		case 'css'||'less'||'sass':
			pathnameList = pathnameList.map(function (pn) {
				return baseUrl + pn;
			});
			break;
		default:
			logger('[info] ext not match');
			return next();
	}

	switch(ext) {
		case 'js':
			logger('[info] combo js '+ req.url);
			comboJS({
				baseUrl: baseUrl,
				optimize: "none",
				findNestedDependencies: true,
				include: pathnameList,
				out: function (outContent) {
					var buf = Buffer(outContent, 'utf8');
					res.writeHead(200, {
						'Content-Type': 'application/javascript; charset=utf-8',
						'Content-Length': Buffer.byteLength(buf), // buf.length,
						'Vary': 'Accept-Encoding',
						'Date': new Date(),
						'Cache-Control': 'no-cache',
						// 'Content-Encoding': 'gzip',
						// 'Cache-Control': 'max-age=315360000',
						// 'Expires': new Date((new Date()).getTime() + (60 * 60 * 1000 * 365 * 10)),
						// 'Age': '300',
						'Connection': 'close',
						'Server':COMBO_SERVER,
						'via': VIA
					});

					res.end(buf);
					logger('[info] from requirejs');
				}
			});
			break;
		case 'less':
		case 'sass':
		case 'css':
			logger('[info] combo css '+ req.url);
			comboCSS( pathnameList, function (err, ret) {
				if (err) {
					logger('[error]' + err);
					return errorHandlers('server error');
				}
				res.writeHead(200, {
					'Content-Type': 'text/css',
					'Content-Length': Buffer.byteLength(ret), // ret.length,
					// 'Content-Encoding': 'gzip',
					'Vary': 'Accept-Encoding',
					'Date': new Date(),
					'Server':COMBO_SERVER,
					'via': VIA
				});
				res.write(ret);
				res.end();
			} );
			break;
		default:
			next();
	}
};