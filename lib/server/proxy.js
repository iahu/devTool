module.exports = function (config) {
	// setup proxy server
	var http = require('http');
	var httpProxy = require('http-proxy');
	var proxy = httpProxy.createProxyServer({});
	var matchRule = require('../modules/match-rule.js');
	var logger = require('../modules/logger.js');
	var rules = config.proxy_rules;
	var hosts = config.proxy_hosts;
	var dns = require('dns');
	var nativeURL = require('url');

	proxy.on('error', function(e) {
		logger('[error]', e);
	});
	http.createServer(function (req, res) {
		var protocol = (req.protocol || 'http:');
		var host = req.headers.host;
		var url = req.url;
		var href = url[0] === '/'? (protocol + '//' + host + url) : url;
		var parsedURL = nativeURL.parse(href);
		var hostname = parsedURL.hostname;

		var match = matchRule(href, rules, hosts);

		if ( match ) {
			logger('[info] match rule #'+ (match.index+1));
			proxy.web(req, res, {
				target: match.target
			});
		} else {
			// logger('original target');
			proxy.web(req, res, {
				target: protocol+ '//'+ host +'/'
			});
		}
	}).listen(config.proxy_port);
	logger('[info] proxy server http://localhost:'+config.proxy_port );
};