module.exports = function (config) {
	var rjs = require('requirejs');
	// var config = {
	// 	baseUrl: baseUrl,
	// 	optimize: "none",
	// 	findNestedDependencies: true,
	// 	include: pathnameList,
	// 	out: typeof callback === 'function' ? callback : function(){}
	// };

	function optimizeCallback (buildResponse) {
		// console.log(buildResponse);
	}
	function errorHandlers (err) {
		var body = (err || 'error').toString();
		var msg = '<html><head><title>500 server error</title></head><body>'+ body +'</body></html>';
		res.writeHead(500, {
			'Content-Type': 'text/html'
		});
		res.end(msg);
	}

	rjs.optimize(config, optimizeCallback, errorHandlers);
}