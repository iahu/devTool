module.exports = function (include, callback) {
	var fs = require('fs');
	var less = require('less');
	var sass = require('node-sass');
	var includeLength = include.length;
	var tmp = '';
	var filesCount = 0;
	var hasError = false;
	include.forEach(function(filename) {
		tmp += '{{'+ filename +'}}\n';
	});

	function insertContent (filename, content) {
		filesCount += 1;
		tmp = tmp.replace('{{'+ filename +'}}', content);
		if (filesCount === includeLength) {
			callback(null, tmp);
		}
	}

	include.forEach(function (filename, idx) {
		if (hasError === true) {
			return;
		}
		fs.readFile(filename, 'utf8', function(err, content){
			if (err) {
				hasError = true;
				return callback(err);
			}
			switch( filename.split('.').pop() ) {
				case 'less':
					less.render(content, {
						filename: filename
					}, function (e, output) {
						if (e) {
							hasError = true;
							return callback(err);
						}
						insertContent(filename, output.css);
					});
					break;
				case 'sass':
					sass.render({
						file: filename,
						data: content
					}, function (e, output) {
						if (e) {
							hasError = true;
							return callback(err);
						}
						insertContent(filename, output.css.toString());
					});
					break;
				case 'css':
					insertContent(filename, content);
					break;
				default:
					insertContent(filename, '');
			}
		});
	});
};