module.exports = function (url, rules, hosts) {
	var r, i, j;
	var nativeURL = require('url');
	var hosts_map = {};
	for (i = 0; i < hosts.length; i++) {
		var host = hosts[i].split(/\s+/);
		var ip = host.shift();
		for (j = 0; j < host.length; j++) {
			hosts_map[host[j]] = ip;	
		}
	}

	for (i = 0; i < rules.length; i++) {
		var rule = rules[i].split(/\s+/);
		var left = rule[0];
		var right = rule[1];
		var parsed;

		left = new RegExp(left);
		if ( url.match( left ) ) {
			var _url = url.replace(left, right);
			parsed = nativeURL.parse(_url);
			r = {
				index: i,
				match: left,
				target: parsed.protocol + '//' + parsed.host
			};
		}
	}

	return r;
};