/*
 *connect setup
 *
 */ 
module.exports = function (config) {
	var http = require('http');
	var connect = require('connect');
	var app = connect();
	var serveIndex = require('serve-index');
	var serveStatic = require('serve-static');
	var compression = require('compression');
	var index = serveIndex(config.cwd);
	var staticServe = serveStatic(config.cwd, {
		index: 'index.html'
		// maxAge: '365 days'
	});
	var logger = require('./modules/logger.js');
	var comboServe = require('./server/combo.js');
	var proxyServer = require('./server/proxy.js');

	app.use(compression());
	if ( config.combo !== false ) {
		logger('[info] used combo server');
		app.use(function (req, res, next) {
			comboServe(req, res, next, config);
		});
	}
	if ( config.static !== false ) {
		logger('[info] used static server');
		app.use(index);
		app.use(staticServe);
	}

	http.createServer(app).on('error', function(e){
		switch(e.code) {
			case 'EACCES':
				logger('请尝试用 sudo 运行命令');
				break;
			case 'EADDRINUSE':
				logger(config.port+' 端口被占用，请使用其它端口');
				break;
			default:
				logger('[error] ', e);
		}
	})
	.listen(config.port);
	logger('[info] devtool server http://localhost:' + config.port);

	if (config.proxy) {
		proxyServer(config);
	}

};
