# devTool
devTool for 乐居网平台组前端

## 说明
提供本地的web前端开发环境。

1. __静态文件服务__ 访问静态资源

2. __combo服务__ combo可支持在线r.js解析，在线编译less/sass的功能
	url示例：
	/public/a.css;/public/b.css
	/public/a.js;/public/b.js
	/public/a.js?r
	/public/a.less
	/public/a.sass

3. __代理服务器服务__ 提供反向代理，设置本地hosts的功能
配置文件示例：
	```
	{
		"proxy_port": 1080,
		"proxy_hosts": [
			"127.0.0.1 www.test.com dev.com"
		],
		"proxy_rules": [
			// rule: match-url target-server
			// The latter rule would overwrite the previous rules
			"http://cdn\\d?.exmaple.com http://11.22.33.44", // #1 remote address
			"http://cdn(\\d)?.exmaple.com/test/(.+) http://cdn$1.test.com:8080" // #2 local address
		]
	}
	```
上面的配置将把`http://cdn.exmaple.com/test/a.js` 指向 `127.0.0.1:8080`， 
而`http://cdn.exmaple.com/a.js` 将指向 `11.22.33.44`。


__注:__ 可以通过在`cwd`目录下建一个devtool.json来覆盖默认配置，修改本地的配置需要重启devtool才生效。

## 安装方法
`npm i -g dev-tool`


## 使用方法
  	Usage: devtool [options] directory
	
	Options:

	  -h, --help                 output usage information
	  -V, --version              output the version number
	  -p, --port <port>          static server port
	  -c, --combo [true|false]   use combo server
	  -s, --static [true|false]  use static server
	  -r, --proxy [false|true]   use proxy server
	  -o, --proxy_port <port>    proxy server port

## todo
 - 支持 mockjson 功能